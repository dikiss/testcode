package com.example.testtvplayer.model

import com.example.testtvplayer.AppConstants

object ApiFactory{

    val placeholderApi : PlaceholderApi = RetrofitFactory.retrofit(AppConstants.JSON_PLACEHOLDER_BASE_URL)
        .create(PlaceholderApi::class.java)

    val tmdbApi : TvApi = RetrofitFactory.retrofit(AppConstants.TMDB_BASE_URL)
        .create(TvApi::class.java)
}