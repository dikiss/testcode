package com.example.testtvplayer.model


import com.example.testtvplayer.data.MovieResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET

interface TvApi{

    @GET("movie/popular")
    fun getPopularMovie(): Deferred<Response<MovieResponse>>
}